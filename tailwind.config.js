module.exports = {
  purge: [],
  theme: {
    extend: {},
    container: {
      padding: '1.5em'
    },
    colors: {
      background: {
        primary: 'var(--bg-background-primary)',
        secondary: 'var(--bg-background-secondary)',
        tertiary: 'var(--bg-background-tertiary)',
        form: 'var(--bg-background-form)',
      },
      copy: {
        primary: 'var(--text-copy-primary)',
        secondary: 'var(--text-copy-hover)',
      },
      transparent: 'transparent',
      black: '#000',
      white: '#fff',
    }
  },
  variants: {},
  plugins: [],
  purge: false,
}
