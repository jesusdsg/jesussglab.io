// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import DefaultLayout from '~/layouts/Default.vue'
import VueScrollTo from 'vue-scrollto'
import VueTyper from 'vue-typed-js'
import BackToTop from 'vue-backtotop'


export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  Vue.use(VueTyper)
  Vue.use(BackToTop)

  //Importing  Fonts
  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css?family=Montserrat&display=fallback'
  })

  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/css?family=Open%20Sans&display=fallback'
  })


  Vue.use(VueScrollTo, {
    duration: 600,
    easing: "ease",
  })

  //Meta
  head.meta.push({
    name: 'description',
    content: 'Jesús Salcedo - Creative web developer'
  })

  head.meta.push({
    name: 'author',
    content: 'Jesús Salcedo'
  })

  head.meta.push({
    name: 'keywords',
    content: 'Jesús Salcedo, Vue, HTML5, CSS, Design, Development, Web, Mobile'
  })

}
